﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace D3QC
{
     /// <summary>
     /// Interaction logic for App.xaml
     /// </summary>
     public partial class App : Application
     {
          #region Fields
          private static Utility _utility = new Utility();

          #endregion

          #region Properties

          public static Utility Utility
          {
               get { return _utility; }
          }

          #endregion

          #region Methods

          internal void Application_Startup( object sender, StartupEventArgs e )
          {
               if ( !AppDataExists() )
               {
                    try
                    {
                         Utility.AutoResolveSettings();
                         Utility.CreateFolders();
                         Utility.WriteSettings();
                         Utility.LoadSettings();
                    }
                    catch ( Exception ex )
                    {
                         MessageBox.Show( ex.Source.ToString() );
                    }  
               }
               else
               {
                    Utility.LoadSettings();
                   
               }
               StartupUri = new Uri( "/D3QC;component/MainWindow.xaml", UriKind.Relative );
          }

          /// <summary>
          /// Checks to see if the app data folder exists.
          /// </summary>
          /// <returns>True if AppData folder exists</returns>
          internal static Boolean AppDataExists()
          {
               StringBuilder p = new StringBuilder();
               p.Append( Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ) );
               p.Append( "//" );
               p.Append( System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString() );
               return System.IO.Directory.Exists( p.ToString() );
          }

          #endregion


     }
}
