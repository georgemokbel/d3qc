﻿using System;

namespace D3QC.Model
{
     /// <summary>
     /// Represents the relevant settings from D3Prefs.txt
     /// </summary>
     public sealed class D3PrefsModel
     {
          #region Fields
          private Int32 displayModeWindowMode;
          private Int32 displayModeWidth;
          private Int32 displayModeHeight;
          private double gamma;
          private static D3PrefsModel instance = null;
          #endregion // Fields

          #region Properties
          public Int32 DisplayModeWindowMode { get { return displayModeWindowMode; } set { displayModeWindowMode = value; } }
          public Int32 DisplayModeWidth { get { return displayModeWidth; } set { displayModeWidth = value; } }
          public Int32 DisplayModeHeight { get { return displayModeHeight; } set { displayModeHeight = value; } }
          public double Gamma { get { return gamma; } set { gamma = value; } }
          public static D3PrefsModel GetInstance { get { if ( instance == null ) instance = new D3PrefsModel(); return instance; } }
          #endregion // Properties

          #region Constructor
          private D3PrefsModel()
          {
               
          }
          #endregion // Constructor
     }
}
