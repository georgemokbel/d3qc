﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace D3QC.Model
{
     /// <summary>
     /// Class represnting the photos to be processed.
     /// </summary>
     public class PhotoModel
     {
          #region Fields
          ObservableCollection<String> _fileList;
          ObservableCollection<String> _processedFileList;
          #endregion

          #region Properties

          /// <summary>
          /// The list of files to be processed.
          /// </summary>
          public ObservableCollection<String> FileList
          {
               get
               {
                    return _fileList;
               }
               set
               {
                    RemoveAllFiles();
                    _fileList = value;
               }
          }

          /// <summary>
          /// The files that have been processed.
          /// </summary>
          public ObservableCollection<String> ProcessedFileList
          {
               get
               {
                    return _processedFileList;
               }
               set
               {
                    RemoveAllProcessedFiles();
                    _processedFileList = value;
               }
          }

          #endregion

          #region Constructor
          public PhotoModel()
          {
               _fileList = new ObservableCollection<String>();
               _processedFileList = new ObservableCollection<String>();
          }
          #endregion

          #region Methods
          public void RemoveAllFiles()
          {
               while ( _fileList.Count > 0 )
               {
                    _fileList.RemoveAt( _fileList.Count - 1 );
               }
          }

          public void RemoveAllProcessedFiles()
          {
               while ( _processedFileList.Count > 0 )
               {
                    _processedFileList.RemoveAt( _processedFileList.Count - 1 );
               }
          }
          #endregion
     }
}
