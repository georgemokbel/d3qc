﻿using System;

namespace D3QC.Model
{
     public class SettingsModel
     {
          #region Fields

          String _d3PrefsFile;
          String _screenshotPath;
          String _croppedScreenshotPath;
          String _originalsScreenshotPath;
          String _fileAppendTag;
          Boolean _loggingStatus;
          Boolean _validD3PrefsFile;
          Boolean _validScreenshotPath;
          Boolean _validCroppedScreenshotPath;
          Boolean _validOriginalsScreenshotPath;

          #endregion

          #region Properties
          /// <summary>
          /// Location of D3Prefs.txt
          /// </summary>
          public String D3PrefsFile
          {
               get
               {
                    return _d3PrefsFile;
               }
               set
               {
                    _d3PrefsFile = value;
               }
          }

          /// <summary>
          /// Location of Diablo III screenshot folder
          /// </summary>
          public String ScreenshotPath
          {
               get
               {
                    return _screenshotPath;
               }

               set
               {
                    _screenshotPath = value;
               }
          }

          /// <summary>
          /// Where to move cropped screenshots
          /// </summary>
          public String CroppedScreenshotPath
          {
               get
               {
                    return _croppedScreenshotPath;
               }
               set
               {
                    _croppedScreenshotPath = value;
               }
          }

          /// <summary>
          /// Where to move copies of original screenshots.
          /// </summary>
          public String OriginalsScreenshotPath
          {
               get
               {
                    return _originalsScreenshotPath;
               }
               set
               {
                    _originalsScreenshotPath = value;
               }
          }

          /// <summary>
          /// Tag to append to a filename before saving.
          /// </summary>
          public String FileAppendTag
          {
               get 
               {
                    return _fileAppendTag;
               }
               set 
               {
                    _fileAppendTag = value;
               }
          }

          /// <summary>
          /// Flag denoting whether logs are being made or not.
          /// </summary>
          public Boolean LoggingStatus 
          {
               get
               {
                    return _loggingStatus;
               }
               set
               {
                    _loggingStatus = value;
               }
          }

          /// <summary>
          /// Is the D3PrefsFile Valid?
          /// </summary>
          public Boolean ValidD3PrefsFile
          {
               get 
               { 
                    return _validD3PrefsFile; 
               }
               set
               {
                    _validD3PrefsFile = value;
               }
          }

          public  Boolean ValidScreenshotPath
          {
               get
               {
                    return _validScreenshotPath;
               }

               set
               {
                    _validScreenshotPath = value;
                   
               }
          }

          public Boolean ValidCroppedScreenshotPath
          {
               get
               {
                    return _validCroppedScreenshotPath;
               }
               set
               {
                    _validCroppedScreenshotPath = value;
               }

          }


          public  Boolean ValidOriginalsScreenshotPath
          {
               get
               {
                    return _validOriginalsScreenshotPath;
               }
               set
               {
                   _validOriginalsScreenshotPath = value;
               }
          }

          #endregion

          #region Constructor
          public SettingsModel()
          {
          }
          #endregion
     }
}
