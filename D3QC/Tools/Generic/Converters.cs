﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
namespace D3QC
{
     public class BooleanConverter<T> : IValueConverter
     {
          public T False { get; set; }
          public T True { get; set; }

          public BooleanConverter( T truthValue, T falseValue )
          {
               True = truthValue;
               False = falseValue;
          }

          public object Convert( object value, Type targetType, object param, System.Globalization.CultureInfo ci )
          {
               return value is Boolean && (( Boolean )value) ? True : False;
          }

          public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
          {
               throw new NotImplementedException();
          }
     }

     [ValueConversion( typeof(Boolean), typeof(Brush) )]
     public class BoolToBorderBrushConverter : BooleanConverter<Brush>
     {
          /// <summary>
          /// True = Transparent
          /// False = Red
          /// </summary>
          public BoolToBorderBrushConverter() : base( new SolidColorBrush( Colors.Transparent ), new SolidColorBrush( Colors.Red ) ) { }

     }

     [ValueConversion( typeof( Boolean ), typeof( Visibility ) )]
     public class BoolToVisibilityConverter : BooleanConverter<Visibility>
     {
          /// <summary>
          /// True = Hidden
          /// False = Visible
          /// </summary>
          public BoolToVisibilityConverter() : base( Visibility.Hidden, Visibility.Visible ) { } 
     }

}
