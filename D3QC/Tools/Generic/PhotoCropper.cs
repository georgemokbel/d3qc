﻿using D3QC.Model;
using TemplateMatching;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;
using System.Windows;

namespace D3QC
{
     /// <summary>
     /// Class that handles the loading of images, their cropping, and saving them.
     /// </summary>
     public sealed class PhotoCropper : IDisposable
     {
          #region Fields
          Bitmap _current;          // Current bitmap being modified.
          Bitmap _xScaledCurrent;    // Scaled version for x searching
          Bitmap _yScaledCurrent;    // Scaled version for y searching
          Bitmap _topTemplate;      // The upper template being searched for.
          Bitmap _botTemplate;      // The lower template being searched for.
          Bitmap _scaledTopTemplate;      // Scaled version of upper template.
          Bitmap _scaledBotTemplate;      // Scaled version of lower template.
          Int32 _xScaleFactor;         // How much we scale width to speed up searching.
          Int32 _yScaleFactor;        // How much we scale height to speed up searching.

          List<String> _photoList; // FileList in PhotoModel
          Boolean _currentCropStatus; // Status of the current crop operation.
          StringBuilder _cropStatusMessage; // Status as a message to be displayed in Default View
          StringBuilder _logMessage; // Message to be displayed in log folder if failed.
          StringBuilder _userStateMessage; // Complete message passed to progress changed method.
          StringBuilder _currentFileName;
          StringBuilder _absoluteCurrentFileName;
          #endregion // Fields

          #region Constructor


          /// <summary>
          /// PhotoCropper constructor.
          /// </summary>
          /// <param name="photoModel">Reference to the applications current PhotoModel as found in the DefaulViewModel</param>
          public PhotoCropper( PhotoModel photoModel )
          {
               if ( GetTemplatePhotos() )
               {
                    GetPhotoScaleFactors();
                    ScaleTemplatePhotos();
               }
               _photoList = new List<String>();
               UpdatePhotoList( photoModel.FileList );
               _currentFileName = new StringBuilder();
               _absoluteCurrentFileName = new StringBuilder();
               _cropStatusMessage = new StringBuilder();
               _userStateMessage = new StringBuilder();
               _logMessage = new StringBuilder();
          }

      

          #endregion // Constructor

          #region Methods

          /// <summary>
          /// IDiposable implementation.
          /// </summary>
          public void Dispose()
          {
               if ( _current != null )
               {
                    _current.Dispose();
               }
               if ( _xScaledCurrent != null )
               {
                    _xScaledCurrent.Dispose();
               }
               if ( _yScaledCurrent != null )
               {
                    _yScaledCurrent.Dispose();
               }
               if ( _topTemplate != null )
               {
                    _topTemplate.Dispose();
               }
               if ( _botTemplate != null )
               {
                    _botTemplate.Dispose();
               }
               if ( _scaledTopTemplate != null )
               {
                    _scaledTopTemplate.Dispose();
               }
               if ( _scaledBotTemplate != null )
               {
                    _scaledBotTemplate.Dispose();
               }

          }


          /// <summary>
          /// Updates the photolist to the current photo model.
          /// </summary>
          /// <param name="updatedPhotoModel">The observable collection from the photo model in the default view model.</param>
          public void UpdatePhotoList( ObservableCollection<String> updatedPhotoModel )
          {
               _photoList.Clear();
               foreach ( var e in updatedPhotoModel )
               {
                    _photoList.Add( e.ToString() );
               }
          }

          /// <summary>
          /// Processes the image in DefaultView's PhotoModel.
          /// </summary>
          /// <param name="backgroundWorker">The threaded background worker thread for the default View.</param>
          public void ProcessImages( BackgroundWorker backgroundWorker )
          {
               
               Int32 currentProgress = 0;
               if ( _photoList.Count > 0 )
               {
                    
                    Int32 maxProgress = _photoList.Count;
                    foreach ( String s in _photoList )
                    {
                         _currentFileName.Clear();
                         _absoluteCurrentFileName.Clear();
                         _cropStatusMessage.Clear();
                         _logMessage.Clear();
                         _userStateMessage.Clear();

                         _currentFileName.Append( s );
                         _absoluteCurrentFileName.Append( ( String )Application.Current.Properties [ "ScreenshotPath" ] );
                         _absoluteCurrentFileName.Append( s );


                         if ( backgroundWorker.CancellationPending )
                         {
                              _cropStatusMessage.Append( "FAIL" );
                              _logMessage.Append( "Operation cancelled while processing " + _currentFileName );
                              return;
                         }
                         else
                         {
                              if ( PrepareCurrentImage( s ) )
                              {

                                   CropCurrentImage();
                                   if ( _currentCropStatus == true )
                                   {
                                        _cropStatusMessage.Append( "OK" );
                                        _logMessage.Append( "Successfully cropped " + _currentFileName );
                                   }
                                   else
                                   {
                                        _cropStatusMessage.Append( "FAIL" );
                                   }
                              }
                              else
                              {
                                   _cropStatusMessage.Append( "FAIL" );
                                   _logMessage.Append( "Failed to prepare " + _currentFileName + " for modification." );
                              }
                              
                         }
                         MoveOriginalImage();
                         ++currentProgress;
                         _userStateMessage.Append( _cropStatusMessage.ToString() );
                         _userStateMessage.Append( '@' );
                         _userStateMessage.Append( _logMessage.ToString() );
                         
                         backgroundWorker.ReportProgress( Convert.ToInt32( (( Decimal )currentProgress / ( Decimal )maxProgress) * 100 ), _userStateMessage.ToString() );  
                    }
               }
               // finally { UnlockTemplateImages(); }
          }

          /// <summary>
          /// Prepares the current image for processing.
          /// </summary>
          /// <returns>True if image is able to be cropped.</returns>
          private Boolean PrepareCurrentImage( String filename )
          {
               Bitmap temp = new Bitmap( _absoluteCurrentFileName.ToString() );
               if ( temp != null )
               {
                    _current = Native.Clone( temp, PixelFormat.Format24bppRgb );
                    temp.Dispose();
               }
               if ( _current != null )
               {
                    return true;
               }
               return false;
          }

          /// <summary>
          /// Tries to crop the current image.
          /// </summary>
          private void CropCurrentImage()
          {
               ScaleCurrentDown();
               Rectangle scaledTopCoordinates = FindTopCoordinates();
               if ( scaledTopCoordinates.X == 0 && scaledTopCoordinates.Y == 0 )
               {
                    _currentCropStatus = false;
               }
               else
               {
                    Rectangle topCoordinates = new Rectangle( scaledTopCoordinates.X * _xScaleFactor, scaledTopCoordinates.Y, scaledTopCoordinates.Width * _xScaleFactor, scaledTopCoordinates.Height );
                    
                    if ( topCoordinates.Width != _topTemplate.Width )
                         topCoordinates.Width = _topTemplate.Width;    // adjust for scaling error

                    Rectangle scaledBotCoordinates = FindBottomCoordinates( topCoordinates ); // Search down y-axis for bottom template coordinates.

                    if ( scaledBotCoordinates.X == 0 && scaledBotCoordinates.Width == 0 )
                    {
                         _currentCropStatus = false;
                    }
                    else
                    {
                         // Scale back up bottom coordinates
                         Rectangle botCoordinates = new Rectangle( scaledBotCoordinates.X, scaledBotCoordinates.Y * _yScaleFactor, scaledBotCoordinates.Width, scaledBotCoordinates.Height * _yScaleFactor );
                         // Get the coordinates based on the original image size.
                         Rectangle imageCoordinates = new Rectangle( topCoordinates.X, topCoordinates.Y, topCoordinates.Width, (botCoordinates.Y + botCoordinates.Height) - topCoordinates.Y );
                         Bitmap croppedImage = _current.Clone( imageCoordinates, _current.PixelFormat );

                         SaveCroppedImage( croppedImage );

                         croppedImage.Dispose();
                         _current.Dispose();
                         _xScaledCurrent.Dispose();
                         _yScaledCurrent.Dispose();

                         
                         _currentCropStatus =  true;
                    }
                   
               }

          }

          
          

          #region Template Loading Method.
          /// <summary>
          /// Loads the appropriate template photos from templates.dll and scales them to half size.
          /// </summary>
          /// <returns>True if successfully loaded template photos.</returns>
          private Boolean GetTemplatePhotos()
          {
               double g = D3PrefsModel.GetInstance.Gamma;
               try
               {
                    if ( g < .625 )
                    {
                         _topTemplate = Native.Clone( templates.Properties.Resources.top50, PixelFormat.Format24bppRgb );
                         _botTemplate = Native.Clone( templates.Properties.Resources.bot50, PixelFormat.Format24bppRgb );
                    }
                    else if ( g >= .625 && g < .875 )
                    {
                         _topTemplate = Native.Clone( templates.Properties.Resources.top75, PixelFormat.Format24bppRgb );
                         _botTemplate = Native.Clone( templates.Properties.Resources.bot75, PixelFormat.Format24bppRgb );
                    }
                    else if ( g >= .875 && g < 1.125 )
                    {
                         _topTemplate = Native.Clone( templates.Properties.Resources.top100, PixelFormat.Format24bppRgb );
                         _botTemplate = Native.Clone( templates.Properties.Resources.bot100, PixelFormat.Format24bppRgb );
                    }
                    else if ( g >= 1.125 && g < 1.375 )
                    {
                         _topTemplate = Native.Clone( templates.Properties.Resources.top125, PixelFormat.Format24bppRgb );
                         _botTemplate = Native.Clone( templates.Properties.Resources.bot125, PixelFormat.Format24bppRgb );
                    }
                    else
                    {
                         _topTemplate = Native.Clone( templates.Properties.Resources.top150, PixelFormat.Format24bppRgb );
                         _botTemplate = Native.Clone( templates.Properties.Resources.bot150, PixelFormat.Format24bppRgb );
                    }
                    return true;
               }
               catch ( Exception ex )
               {
                    Messenger.Default.Send( "Failed to load template photos, are you sure you have a templates.dll file?" , D3QC.ViewModel.MessageType.LogMessage );
               }
               return false;
          }

          #endregion // Template Loading Method

          #region Coordinate Location Methods
          /// <summary>
          /// Searches for the top template image.
          /// </summary>
          /// <returns>Zero rectangle if failed to find match, scaled down top coordinates otherwise.</returns>
          private Rectangle FindTopCoordinates()
          {
               ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching( .95f );

               TemplateMatch [] matches = tm.ProcessImage( _xScaledCurrent, _scaledTopTemplate );

               if ( matches.Length == 0 )
               {
                    _logMessage.Append( "Failed to crop " + _currentFileName + ", could not find a top template match." );
                    return new Rectangle( 0, 0, 0, 0 );
               }
               // Returns the best match which is located at zero index.
               return matches [ 0 ].Rectangle;
          }

          /// <summary>
          /// Searches for the bottom template image.
          /// </summary>
          /// <param name="topCoordinates">The absolute coordinates of the top template image.</param>
          /// <returns>Zero rectangle if failed to find match, scaled down bottom coordinates otherwise.</returns>
          private Rectangle FindBottomCoordinates( Rectangle topCoordinates )
          {
               topCoordinates.Y /= _yScaleFactor;
               topCoordinates.Height = _yScaledCurrent.Height - topCoordinates.Y;

               ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching( .95f );

               TemplateMatch [] matches = tm.ProcessImage( _yScaledCurrent, _scaledBotTemplate, topCoordinates );

               if ( matches.Length == 0 )
               {
                    _logMessage.Append( "Failed to crop " + _currentFileName + ", could not find a bottom template match." );
                    return new Rectangle( 0, 0, 0, 0 );
               }
               return matches [ 0 ].Rectangle;
          }


          
          #endregion

          #region File Saving / Moving Methods

          /// <summary>
          /// Saves the successfully cropped image to destination folder.
          /// </summary>
          /// <param name="croppedImage">The cropped version of the current image.</param>
          private void SaveCroppedImage( Bitmap croppedImage )
          {
               StringBuilder fn = new StringBuilder();
               fn.Append( ( String )Application.Current.Properties [ "CroppedScreenshotPath" ] );
               fn.Append( "\\" + DateTime.Now.ToString( "hhmm" ) + _currentFileName.ToString() );
               croppedImage.Save( fn.ToString() );
          }


          /// <summary>
          /// Moves the successfully cropped image to designated folder.
          /// </summary>
          private void MoveOriginalImage()
          {
               StringBuilder fn = new StringBuilder();
               fn.Append( ( String )Application.Current.Properties [ "OriginalsScreenshotPath" ] );
               fn.Append( DateTime.Now.ToString( "hhmm" ) + _currentFileName.ToString() );
               System.IO.File.Move( _absoluteCurrentFileName.ToString(), fn.ToString() );
          }

          #endregion // File Saving / Moving Methods

          #region Scaling Methods
          /// <summary>
          /// Sets the scale factor depending on the width used in D3Prefs.txt
          /// </summary>
          /// <remarks>Scale factors are set to what they are based on the template sizes. Might seem weird but since the bitmaps sizes are ints we have to it this way else loss of precision ruins affects template matching accuracy.</remarks>
          private void GetPhotoScaleFactors()
          {
               Int32 w = D3PrefsModel.GetInstance.DisplayModeWidth;
               Int32 h = D3PrefsModel.GetInstance.DisplayModeHeight;
               if ( w == 2560 )
               {
                    _xScaleFactor = 8;
                    _yScaleFactor = 2;
               }
               else if ( w == 1920 && h == 1200 )
               {
                    _xScaleFactor = 6;
                    _yScaleFactor = 2;
               }
               else if ( w == 1920 && h == 1080 )
               {
                    // tempalte photo for 1920x1080 is 426x16, need an Int32 that divides evenly.
                    _xScaleFactor = 12;
                    _yScaleFactor = 2;
               }
               else
               {
                    _xScaleFactor = 1;
                    _yScaleFactor = 1;
               }

          }

          /// <summary>
          /// Scales the template photos to speed up searching, scaling based on GetScaleFactor()
          /// </summary>
          private void ScaleTemplatePhotos()
          {
               Int32 width = _topTemplate.Width / _xScaleFactor;
               ResizeTransform widthTransform = new ResizeTransform( width, _topTemplate.Height );
               _scaledTopTemplate = widthTransform.Apply( _topTemplate );

               Int32 height = _botTemplate.Height / _yScaleFactor;
               ResizeTransform heightTransform = new ResizeTransform( _botTemplate.Width, height );
               _scaledBotTemplate = heightTransform.Apply( _botTemplate );
          }

          /// <summary>
          /// Scales the image being processed and applies it to temporaryCurrent, scaling based on GetScaleFactor()
          /// </summary>
          private void ScaleCurrentDown()
          {
               Int32 width = _current.Width / _xScaleFactor;
               ResizeTransform widthTransform = new ResizeTransform( width, _current.Height );
               _xScaledCurrent = widthTransform.Apply( _current );

               Int32 height = _current.Height / _yScaleFactor;
                ResizeTransform heightTransform = new ResizeTransform( _current.Width, height );
               _yScaledCurrent = heightTransform.Apply( _current );

          }
          #endregion // Scaling methods.
          
       
           #endregion // Methods


     }
}

