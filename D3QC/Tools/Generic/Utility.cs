﻿using D3QC.Model;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace D3QC
{
     /// <summary>
     /// Utility handles reading and writing of configuration files.
     /// </summary>
     public sealed class Utility
     {

          #region Methods
          /// <summary>
          /// Loads the settings from the config file located in the AppData folder, fills D3Prefs
          /// </summary>
          /// <remarks>Throws</remarks>
          public void LoadSettings()
          {
               try
               {
                    String configFile = GetAppDataFolder() + "\\config.ini";

                    using ( StreamReader reader = File.OpenText( configFile ) )
                    {

                         StringBuilder info = new StringBuilder();
                         // D3Prefs Path
                         info.Append( reader.ReadLine() );
                         Application.Current.Properties [ "D3PrefsFile" ] = ParseLine( info.ToString() );
                         // Screenshot Path
                         info.Clear();
                         info.Append( reader.ReadLine() );
                         Application.Current.Properties [ "ScreenshotPath" ] = ParseLine( info.ToString() );
                         // Cropped Path
                         info.Clear();
                         info.Append( reader.ReadLine() );
                         Application.Current.Properties [ "CroppedScreenshotPath" ] = ParseLine( info.ToString() );
                         // Originals Path
                         info.Clear();
                         info.Append( reader.ReadLine() );
                         Application.Current.Properties [ "OriginalsScreenshotPath" ] = ParseLine( info.ToString() );
                    }

                    ReadD3Prefs();
               }
               catch ( Exception ex )
               {
                    throw new Exception( "There was an error reading the configuration file.", ex );
               }
          }

          /// <summary>
          /// Writes the settings into a file config.ini located in the AppData Folder.
          /// </summary>
          /// <remarks>Throws</remarks>
          public void WriteSettings()
          {
               String configFile = GetAppDataFolder() + "\\config.ini";
               String configData = BuildConfigFileData();
               try
               {
                    using ( FileStream fs = File.Create( configFile ) )
                    {
                         Byte [] data = new UTF8Encoding( true ).GetBytes( configData );
                         fs.Write( data, 0, data.Length );
                         Array.Clear( data, 0, data.Length );
                    }
               }
               catch ( Exception ex )
               {
                    throw new Exception( "There was an error writing to the configuration file.", ex );
               }
          }

          /// <summary>
          /// Tries to create AppData Folder, Cropped folder and Originals folder
          /// </summary>
          /// <remarks>Throws</remarks>
          public void CreateFolders()
          {
               CreateFolder( GetAppDataFolder() );
               CreateFolder( ( String )Application.Current.Properties [ "CroppedScreenshotPath" ] );
               CreateFolder( ( String )Application.Current.Properties [ "OriginalsScreenshotPath" ] );
          }

          /// <summary>
          /// Attemps to resolve settings.
          /// </summary>
          public void AutoResolveSettings()
          {
               LocateD3PrefsFile();
               LocateD3ScreenshotFolder();
               FillPathsRelativeToScreenshotFolder();
          }

          /// <summary>
          /// Locates D3Prefs.txt and sets the corresponding application property.
          /// </summary>
          /// <remarks>Throws</remarks>
          private void LocateD3PrefsFile()
          {
               String path = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ) + "\\Diablo III\\D3Prefs.txt";
               if ( File.Exists( path ) )
               {
                    Application.Current.Properties [ "D3PrefsFile" ] = path;
               }
               else
               {
                    throw new Exception( "Failed to auto resolve D3Prefs.txt file, it is not located in the default directory." );
               }

          }

          /// <summary>
          /// Locates default Diablo III screenshot folder and sets the corresponding application property.
          /// </summary>
          /// <remarks>Throws</remarks>
          private void LocateD3ScreenshotFolder()
          {
               String path = Environment.GetFolderPath( Environment.SpecialFolder.MyDocuments ) + "\\Diablo III\\";
               if ( Directory.Exists( path ) )
               {
                    Application.Current.Properties [ "ScreenshotPath" ] = path;
               }
               else
               {
                    throw new Exception( "Failed to auto resolve Diablo III screenshot folder, it is not located in the default directory." );
               }
          }

          /// <summary>
          /// Sets CroppedScreenshotPath and OriginalsScreenshotPath properties relative to ScreenshotPath property.
          /// </summary>
          /// <remarks> CroppedScreenshotPath is \Cropped\, Originals ScreenshotPath is \Originals\</remarks>
          private void FillPathsRelativeToScreenshotFolder()
          {
               String screenshotPath = ( String )Application.Current.Properties [ "ScreenshotPath" ];
               Application.Current.Properties [ "CroppedScreenshotPath" ] = screenshotPath + "Cropped\\";
               Application.Current.Properties [ "OriginalsScreenshotPath" ] = screenshotPath + "Originals\\";
          }

          /// <summary>
          /// Gets the app data folder path.
          /// </summary>
          /// <returns>Path location of AppData folder.</returns>
          private String GetAppDataFolder()
          {
               StringBuilder appDataFolder = new StringBuilder();
               appDataFolder.Append( Environment.GetFolderPath( Environment.SpecialFolder.ApplicationData ) );
               appDataFolder.Append( "\\" );
               appDataFolder.Append( System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString() );
               return appDataFolder.ToString();
          }

          /// <summary>
          /// Creates a folder if it doesn't already exist.
          /// </summary>
          /// <param name="path">The location where the folder will be created.</param>
          private void CreateFolder(String path)
          {
               String folder = path;
               if( !(Directory.Exists( folder ) ) )
               {
                    try
                    {
                         DirectoryInfo dir = Directory.CreateDirectory( folder );
                    }
                    catch ( Exception ex )
                    {
                         if ( ex.InnerException.GetType() == typeof( UnauthorizedAccessException ) )
                         {
                              throw new Exception("Insufficient priviliges, failed to create " + path, ex);
                         }
                    }
               }
          }

          /// <summary>
          /// Builds config file from Application properties.
          /// </summary>
          /// <returns>Configuration file as string.</returns>
          private String BuildConfigFileData()
          {        
               
               StringBuilder config = new StringBuilder();
               config.Append( "D3Prefs Location:" );
               config.Append( ( String )Application.Current.Properties [ "D3PrefsFile" ] );
               config.Append( Environment.NewLine );
               config.Append( "Screenshot Path:" );
               config.Append( ( String )Application.Current.Properties [ "ScreenshotPath" ] );
               config.Append( Environment.NewLine );
               config.Append( "Cropped Screenshot Path:" );
               config.Append( ( String )Application.Current.Properties [ "CroppedScreenshotPath" ] );
               config.Append( Environment.NewLine );
               config.Append( "Processed Screenshot Path:" );
               config.Append( ( String )Application.Current.Properties [ "OriginalsScreenshotPath" ] );
               config.Append( Environment.NewLine );
               return config.ToString();
          }

          /// <summary>
          /// Parses a line from the config.ini file. This is a bit hackish.
          /// </summary>
          /// <param name="line">Line to be parsed</param>
          /// <returns>The relevant part of the config data.</returns>
          private String ParseLine( String line )
          {
               String [] lines = line.Split( ':' );
               StringBuilder valid = new StringBuilder();
               if ( lines.Length > 2 )
               {

                    valid.Append( lines [ 1 ] );
                    valid.Append( ':' );
                    valid.Append( lines [ 2 ] );
               }
               return valid.ToString();
          }

          /// <summary>
          /// Reads the D3Prefs.txt and stores values in the D3PrefsModel class.
          /// </summary>
          /// <returns>True if successfully read the D3Prefs.txt files. Failure to read the file results in application shutdown.</returns>
          public static Boolean ReadD3Prefs()
          {
               if( String.IsNullOrEmpty( (String) Application.Current.Properties [ "D3PrefsFile" ] ) )
               {
                    Messenger.Default.Send( "Failed to read from D3Prefs.txt, file does not exist. Please configure path settings.", D3QC.ViewModel.MessageType.LogMessage );
                    Messenger.Default.Send( false, D3QC.ViewModel.MessageType.SettingsMessage ); // Disables the controls on the main window.
                    return false;
               }
               else 
               {
                    String d3prefs = ( String )Application.Current.Properties [ "D3PrefsFile" ]; 
                     if ( File.Exists( d3prefs ) )
                     {
                          using ( StreamReader reader = File.OpenText( d3prefs ) )
                          {
                               while ( reader.Peek() >= 0 )
                               {
                                    // String line = reader.ReadLine();
                                    String line = reader.ReadLine();
                                    if ( line.StartsWith( "DisplayModeWindowMode" ) )
                                    {
                                         String d = Regex.Match( line, @"\d+" ).Value;
                                         Int32 v;
                                         Int32.TryParse( d, out v );
                                         D3PrefsModel.GetInstance.DisplayModeWindowMode = v;

                                    }
                                    else if ( line.StartsWith( "DisplayModeWidth" ) )
                                    {
                                         String d = Regex.Match( line, @"\d+" ).Value;
                                         Int32 v;
                                         Int32.TryParse( d, out v );
                                         D3PrefsModel.GetInstance.DisplayModeWidth = v;
                                    }
                                    else if ( line.StartsWith( "DisplayModeHeight" ) )
                                    {
                                         String d = Regex.Match( line, @"\d+" ).Value;
                                         Int32 v;
                                         Int32.TryParse( d, out v );
                                         D3PrefsModel.GetInstance.DisplayModeHeight = v;
                                    }
                                    else if ( line.StartsWith( "Gamma" ) )
                                    {

                                         String d = Regex.Match( line, @"\d\D\d+" ).Value;
                                         double v;
                                         Double.TryParse( d, out v );
                                         D3PrefsModel.GetInstance.Gamma = v;
                                    }
                               }
                          }
                          return true;
                     }
                     else
                     {
                          // D3Prefs.txt does not exist or settings are not yet set.
                          Messenger.Default.Send( "Failed to read from D3Prefs.txt, file does not exist. Please configure path settings.", D3QC.ViewModel.MessageType.LogMessage );
                          Messenger.Default.Send( false, D3QC.ViewModel.MessageType.SettingsMessage ); // Disables the controls on the main window.
                          return false;
                     }
               }
               
          }
          #endregion
     }

}
