﻿// Slightly modified and stripped down version of AForge.net
// http://www.aforgenet.com/framework/

namespace TemplateMatching
{
     using System;
     using System.Drawing;
     using System.Drawing.Imaging;
     using System.Collections.Generic;

     public class ExhaustiveTemplateMatching
     {
          private float _similarity = .95f;

          public float Similarity
          {
               get { return _similarity; }
               set { _similarity = Math.Min( 1, Math.Max( 0, value ) ); }
          }

          public ExhaustiveTemplateMatching( float similarity )
          {
               _similarity = similarity;
          }

          public TemplateMatch [] ProcessImage( Bitmap image, Bitmap template )
          {
               return ProcessImage( image, template, new Rectangle( 0, 0, image.Width, image.Height ) );
          }

          public TemplateMatch [] ProcessImage( Bitmap image, Bitmap template, Rectangle searchZone )
          {
               if ( ((image.PixelFormat != PixelFormat.Format8bppIndexed) && (image.PixelFormat != PixelFormat.Format24bppRgb))
                    || (image.PixelFormat != template.PixelFormat) )
               {
                    throw new ArgumentException( "Unsupported pixel format of the source or template image." );
               }

               if ( template.Width > image.Width || template.Height > image.Height )
               {
                    throw new ArgumentException( "Template's size should be smaller or equal to the search zone." );
               }

               // lock source and template images
               BitmapData imageData = image.LockBits(
                   new Rectangle( 0, 0, image.Width, image.Height ),
                   ImageLockMode.ReadOnly, image.PixelFormat );
               BitmapData templateData = template.LockBits(
                   new Rectangle( 0, 0, template.Width, template.Height ),
                   ImageLockMode.ReadOnly, template.PixelFormat );

               TemplateMatch [] matchArray;

               try
               {
                    matchArray = ProcessImage( new UImage( imageData ), new UImage( templateData ), searchZone );
               }
               finally
               {
                    image.UnlockBits( imageData );
                    template.UnlockBits( templateData );
               }

               return matchArray;
          }

          public TemplateMatch [] ProcessImage( BitmapData imageData, BitmapData templateData )
          {
               return ProcessImage( new UImage( imageData ), new UImage( templateData ), new Rectangle( 0, 0, imageData.Width, imageData.Height ) );
          }

          public TemplateMatch [] ProcessImage( BitmapData imageData, BitmapData templateData, Rectangle searchZone )
          {
               return ProcessImage( new UImage( imageData ), new UImage( templateData ), searchZone );
          }


          public TemplateMatch [] ProcessImage( UImage image, UImage template, Rectangle searchZone )
          {
               if ( ((image.PixelFormat != PixelFormat.Format8bppIndexed) && (image.PixelFormat != PixelFormat.Format24bppRgb))
                    || (image.PixelFormat != template.PixelFormat) )
               {
                    throw new ArgumentException( "Unsupported pixel format of the source or template image." );
               }

               Rectangle z = searchZone;
               z.Intersect( new Rectangle( 0, 0, image.Width, image.Height ) );

               Int32 startX = z.X;
               Int32 startY = z.Y;

               Int32 sourceWidth = z.Width;
               Int32 sourceHeight = z.Height;
               Int32 templateWidth = template.Width;
               Int32 templateHeight = template.Height;

               if ( templateWidth > sourceWidth || templateHeight > sourceHeight )
               {
                    throw new ArgumentException( "Template's size should be smaller or equal to the search zone." );
               }

               Int32 pixelSize = (image.PixelFormat == PixelFormat.Format8bppIndexed) ? 1 : 3;
               Int32 sourceStride = image.Stride;

               Int32 mapWidth = sourceWidth - templateWidth + 1;
               Int32 mapHeight = sourceHeight - templateHeight + 1;
               Int32 [ , ] map = new Int32 [ mapHeight + 4, mapWidth + 4 ];

               Int32 maxDiff = templateWidth * templateHeight * pixelSize * 255;

               Int32 threshold = ( Int32 )(_similarity * maxDiff);
               Int32 templateWidthInBytes = templateWidth * pixelSize;

               unsafe
               {
                    Byte* baseSrc = ( Byte* )image.Data.ToPointer();
                    Byte* baseTpl = ( Byte* )template.Data.ToPointer();

                    Int32 sourceOffset = image.Stride - templateWidth * pixelSize;
                    Int32 templateOffset = template.Stride - templateWidth * pixelSize;

                    // for each row of the source image
                    for ( Int32 y = 0; y < mapHeight; y++ )
                    {
                         // for each pixel of the source image
                         for ( Int32 x = 0; x < mapWidth; x++ )
                         {
                              Byte* src = baseSrc + sourceStride * (y + startY) + pixelSize * (x + startX);
                              Byte* tpl = baseTpl;

                              // compare template with source image starting from current X,Y
                              Int32 dif = 0;

                              // for each row of the template
                              for ( Int32 i = 0; i < templateHeight; i++ )
                              {
                                   // for each pixel of the template
                                   for ( Int32 j = 0; j < templateWidthInBytes; j++, src++, tpl++ )
                                   {
                                        Int32 d = *src - *tpl;
                                        if ( d > 0 )
                                        {
                                             dif += d;
                                        }
                                        else
                                        {
                                             dif -= d;
                                        }
                                   }
                                   src += sourceOffset;
                                   tpl += templateOffset;
                              }

                              // templates similarity
                              Int32 sim = maxDiff - dif;

                              if ( sim >= threshold )
                                   map [ y + 2, x + 2 ] = sim;
                         }
                    }
               }

               List<TemplateMatch> matchList = new List<TemplateMatch>();

               for ( Int32 y = 2, maxY = mapHeight + 2; y < maxY; ++y )
               {
                    // for each pixel
                    for ( Int32 x = 2, maxX = mapWidth + 2; x < maxX; x++ )
                    {
                         Int32 currentValue = map [ y, x ];

                         // for each windows' row
                         for ( Int32 i = -2; (currentValue != 0) && (i <= 2); i++ )
                         {
                              // for each windows' pixel
                              for ( Int32 j = -2; j <= 2; j++ )
                              {
                                   if ( map [ y + i, x + j ] > currentValue )
                                   {
                                        currentValue = 0;
                                        break;
                                   }
                              }
                         }

                         // check if this point is really interesting
                         if ( currentValue != 0 )
                         {
                              matchList.Add( new TemplateMatch(
                                  new Rectangle( x - 2 + startX, y - 2 + startY, templateWidth, templateHeight ),
                                  ( float )currentValue / maxDiff ) );
                         }
                    }
               }

               TemplateMatch [] matchArray = new TemplateMatch [ matchList.Count ];
               matchList.CopyTo( matchArray );
               Array.Sort( matchArray, new MatchSorter() );
               return matchArray;
          }

          public TemplateMatch [] ProcessImageOptimized( Bitmap image, Bitmap template )
          {
               return ProcessImageOptimized( image, template, new Rectangle( 0, 0, image.Width, image.Height ) );
          }

          public TemplateMatch [] ProcessImageOptimized( Bitmap image, Bitmap template, Rectangle searchZone )
          {
               if ( ((image.PixelFormat != PixelFormat.Format8bppIndexed) && (image.PixelFormat != PixelFormat.Format24bppRgb))
                    || (image.PixelFormat != template.PixelFormat) )
               {
                    throw new ArgumentException( "Unsupported pixel format of the source or template image." );
               }

               if ( template.Width > image.Width || template.Height > image.Height )
               {
                    throw new ArgumentException( "Template's size should be smaller or equal to the search zone." );
               }

               // lock source and template images
               BitmapData imageData = image.LockBits(
                   new Rectangle( 0, 0, image.Width, image.Height ),
                   ImageLockMode.ReadOnly, image.PixelFormat );
               BitmapData templateData = template.LockBits(
                   new Rectangle( 0, 0, template.Width, template.Height ),
                   ImageLockMode.ReadOnly, template.PixelFormat );

               TemplateMatch [] matchArray;

               try
               {
                    matchArray = ProcessImageOptimized( new UImage( imageData ), new UImage( templateData ), searchZone );
               }
               finally
               {
                    image.UnlockBits( imageData );
                    template.UnlockBits( templateData );
               }

               return matchArray;
          }

          public TemplateMatch [] ProcessImageOptimized( BitmapData imageData, BitmapData templateData )
          {
               return ProcessImageOptimized( new UImage( imageData ), new UImage( templateData ), new Rectangle( 0, 0, imageData.Width, imageData.Height ) );
          }

          public TemplateMatch [] ProcessImageOptimized( BitmapData imageData, BitmapData templateData, Rectangle searchZone )
          {
               return ProcessImageOptimized( new UImage( imageData ), new UImage( templateData ), searchZone );
          }

          public TemplateMatch [] ProcessImageOptimized( UImage image, UImage template, Rectangle searchZone )
          {
               if ( ((image.PixelFormat != PixelFormat.Format8bppIndexed) && (image.PixelFormat != PixelFormat.Format24bppRgb))
                    || (image.PixelFormat != template.PixelFormat) )
               {
                    throw new ArgumentException( "Unsupported pixel format of the source or template image." );
               }

               Rectangle z = searchZone;
               z.Intersect( new Rectangle( 0, 0, image.Width, image.Height ) );

               Int32 startX = z.X;
               Int32 startY = z.Y;

               Int32 sourceWidth = z.Width;
               Int32 sourceHeight = z.Height;
               Int32 templateWidth = template.Width;
               Int32 templateHeight = template.Height;

               if ( templateWidth > sourceWidth || templateHeight > sourceHeight )
               {
                    throw new ArgumentException( "Template's size should be smaller or equal to the search zone." );
               }

               Int32 pixelSize = (image.PixelFormat == PixelFormat.Format8bppIndexed) ? 1 : 3;
               Int32 sourceStride = image.Stride;

               Int32 mapWidth = sourceWidth - templateWidth + 1;
               Int32 mapHeight = sourceHeight - templateHeight + 1;

               Int32 [] [] map = new Int32 [ mapHeight + 4 ] [];
               for ( Int32 i = 0; i < mapHeight + 4; ++i )
               {
                    map [ i ] = new Int32 [ mapWidth + 4 ];
               }

               Int32 maxDiff = templateWidth * templateHeight * pixelSize * 255;

               Int32 threshold = ( Int32 )(_similarity * maxDiff);
               Int32 templateWidthInBytes = templateWidth * pixelSize;

               unsafe
               {
                    Byte* baseSrc = ( Byte* )image.Data.ToPointer();
                    Byte* baseTpl = ( Byte* )template.Data.ToPointer();

                    Int32 sourceOffset = image.Stride - templateWidth * pixelSize;
                    Int32 templateOffset = template.Stride - templateWidth * pixelSize;

                    // for each row of the source image
                    for ( Int32 y = 0; y < mapHeight; y++ )
                    {
                         // for each pixel of the source image
                         for ( Int32 x = 0; x < mapWidth; x++ )
                         {
                              Byte* src = baseSrc + sourceStride * (y + startY) + pixelSize * (x + startX);
                              Byte* tpl = baseTpl;

                              // compare template with source image starting from current X,Y
                              Int32 dif = 0;

                              // for each row of the template
                              for ( Int32 i = 0; i < templateHeight; i++ )
                              {
                                   // for each pixel of the template
                                   for ( Int32 j = 0; j < templateWidthInBytes; j++, src++, tpl++ )
                                   {
                                        Int32 d = *src - *tpl;
                                        if ( d > 0 )
                                        {
                                             dif += d;
                                        }
                                        else
                                        {
                                             dif -= d;
                                        }
                                   }
                                   src += sourceOffset;
                                   tpl += templateOffset;
                              }

                              // templates similarity
                              Int32 sim = maxDiff - dif;

                              if ( sim >= threshold )
                              {
                                   map [ y + 2 ] [ x + 2 ] = sim;
                                   x = mapWidth;
                                   y = mapHeight;
                              }
                         }
                    }
               }

               List<TemplateMatch> matchList = new List<TemplateMatch>();

               for ( Int32 y = 2, maxY = mapHeight + 2; y < maxY; ++y )
               {
                    // for each pixel
                    for ( Int32 x = 2, maxX = mapWidth + 2; x < maxX; x++ )
                    {
                         Int32 currentValue = map [ y ] [ x ];

                         // for each windows' row
                         for ( Int32 i = -2; (currentValue != 0) && (i <= 2); i++ )
                         {
                              // for each windows' pixel
                              for ( Int32 j = -2; j <= 2; j++ )
                              {
                                   if ( map [ y + i ] [ x + j ] > currentValue )
                                   {
                                        currentValue = 0;
                                        break;
                                   }
                              }
                         }

                         // check if this point is really interesting
                         if ( currentValue != 0 )
                         {
                              matchList.Add( new TemplateMatch(
                                  new Rectangle( x - 2 + startX, y - 2 + startY, templateWidth, templateHeight ),
                                  ( float )currentValue / maxDiff ) );
                         }
                    }
               }

               TemplateMatch [] matchArray = new TemplateMatch [ matchList.Count ];
               matchList.CopyTo( matchArray );
               Array.Sort( matchArray, new MatchSorter() );
               return matchArray;
          }

          private class MatchSorter : System.Collections.IComparer
          {
               public Int32 Compare( Object x, Object y )
               {
                    float similarityDifference = (( TemplateMatch )y).Similarity - (( TemplateMatch )x).Similarity;
                    // if y has higher similarity return true, else if y's similarity is negative return -1, else return 0.
                    return (similarityDifference > 0) ? 1 : (similarityDifference < 0) ? -1 : 0;
               }
          }

          #region Custom Template Matching
          public TemplateMatch ProcessTopTemplate( BitmapData imageData, BitmapData templateData )
          {
               return ProcessTopTemplate( imageData, templateData, new Rectangle( 0, 0, imageData.Width, imageData.Height ) );
          }

          public TemplateMatch ProcessTopTemplate( BitmapData imageData, BitmapData templateData, Rectangle searchZone )
          {
               return ProcessTopTemplate( new UImage( imageData ), new UImage( templateData ), searchZone );
          }

          public TemplateMatch ProcessTopTemplate( UImage image, UImage template, Rectangle searchZone )
          {
               if ( ((image.PixelFormat != PixelFormat.Format8bppIndexed) && (image.PixelFormat != PixelFormat.Format24bppRgb))
                   || (image.PixelFormat != template.PixelFormat) )
               {
                    throw new ArgumentException( "Unsupported pixel format of the source or template image." );
               }

               Rectangle z = searchZone;
               z.Intersect( new Rectangle( 0, 0, image.Width, image.Height ) );

               Int32 startX = z.X;
               Int32 startY = z.Y;

               Int32 sourceWidth = z.Width;
               Int32 sourceHeight = z.Height;
               Int32 templateWidth = template.Width;
               Int32 templateHeight = template.Height;

               if ( templateWidth > sourceWidth || templateHeight > sourceHeight )
               {
                    throw new ArgumentException( "Template's size should be smaller or equal to the search zone." );
               }

               Int32 pixelSize = (image.PixelFormat == PixelFormat.Format8bppIndexed) ? 1 : 3;
               Int32 sourceStride = image.Stride;

               Int32 mapWidth = sourceWidth - templateWidth + 1;
               Int32 mapHeight = sourceHeight - templateHeight + 1;
               Int32 [ , ] map = new Int32 [ mapHeight + 4, mapWidth + 4 ];

               Int32 maxDiff = templateWidth * templateHeight * pixelSize * 255;

               Int32 threshold = ( Int32 )(_similarity * maxDiff);
               Int32 templateWidthInBytes = templateWidth * pixelSize;


               unsafe
               {
                    Byte* baseSrc = ( Byte* )image.Data.ToPointer();
                    Byte* baseTpl = ( Byte* )template.Data.ToPointer();

                    Int32 sourceOffset = image.Stride - templateWidth * pixelSize;
                    Int32 templateOffset = template.Stride - templateWidth * pixelSize;

                    // for each row of the source image
                    for ( Int32 y = 0; y < mapHeight; y++ )
                    {
                         // for each pixel of the source image
                         for ( Int32 x = 0; x < mapWidth; x++ )
                         {
                              Byte* src = baseSrc + sourceStride * (y + startY) + pixelSize * (x + startX);
                              Byte* tpl = baseTpl;

                              // compare template with source image starting from current X,Y
                              Int32 dif = 0;

                              // for each row of the template
                              for ( Int32 i = 0; i < templateHeight; i++ )
                              {
                                   // for each pixel of the template
                                   for ( Int32 j = 0; j < templateWidthInBytes; j++, src++, tpl++ )
                                   {
                                        Int32 d = *src - *tpl;
                                        if ( d > 0 )
                                        {
                                             dif += d;
                                        }
                                        else
                                        {
                                             dif -= d;
                                        }
                                   }
                                   src += sourceOffset;
                                   tpl += templateOffset;
                              }

                              // templates similarity
                              Int32 sim = maxDiff - dif;

                              if ( sim >= threshold )
                                   map [ y + 2, x + 2 ] = sim;
                         }
                    }
               }

               List<TemplateMatch> matchList = new List<TemplateMatch>();

               for ( Int32 y = 2, maxY = mapHeight + 2; y < maxY; ++y )
               {
                    // for each pixel
                    for ( Int32 x = 2, maxX = mapWidth + 2; x < maxX; x++ )
                    {
                         Int32 currentValue = map [ y, x ];

                         // for each windows' row
                         for ( Int32 i = -2; (currentValue != 0) && (i <= 2); i++ )
                         {
                              // for each windows' pixel
                              for ( Int32 j = -2; j <= 2; j++ )
                              {
                                   if ( map [ y + i, x + j ] > currentValue )
                                   {
                                        currentValue = 0;
                                        break;
                                   }
                              }
                         }

                         // check if this point is really interesting
                         if ( currentValue != 0 )
                         {
                              matchList.Add( new TemplateMatch(
                                  new Rectangle( x - 2 + startX, y - 2 + startY, templateWidth, templateHeight ),
                                  ( float )currentValue / maxDiff ) );
                         }
                    }
               }

               TemplateMatch [] matchArray = new TemplateMatch [ matchList.Count ];
               matchList.CopyTo( matchArray );
               Array.Sort( matchArray, new MatchSorter() );
               return matchArray [ 0 ];
          }
          #endregion
     }
}
