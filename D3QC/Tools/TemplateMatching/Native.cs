﻿// Slightly modified and stripped down version of AForge.net
// http://www.aforgenet.com/framework/

namespace TemplateMatching
{
     using System;
     using System.Runtime.InteropServices;
     using System.Drawing;
     using System.Drawing.Imaging;

     public static class Native
     {
          #region Memory Related
          public static IntPtr CopyUnmanagedMemory( IntPtr dst, IntPtr src, Int32 count )
          {
               unsafe
               {
                    CopyUnmanagedMemory( ( Byte* )dst.ToPointer(), ( Byte* )src.ToPointer(), count );
               }
               return dst;
          }

          public static unsafe Byte* CopyUnmanagedMemory( Byte* dst, Byte* src, Int32 count )
          {
               return memcpy( dst, src, count );
          }

          public static IntPtr SetUnmanagedMemory( IntPtr dst, Int32 filler, Int32 count )
          {
               unsafe
               {
                    SetUnmanagedMemory( ( Byte* )dst.ToPointer(), filler, count );
               }
               return dst;
          }

          public static unsafe Byte* SetUnmanagedMemory( Byte* dst, Int32 filler, Int32 count )
          {
               return memset( dst, filler, count );
          }

          #region memcpy/memset imports

          [DllImport( "ntdll.dll", CallingConvention = CallingConvention.Cdecl )]
          private static unsafe extern Byte* memcpy( Byte* dst, Byte* src, Int32 count );
          [DllImport( "ntdll.dll", CallingConvention = CallingConvention.Cdecl )]
          private static unsafe extern Byte* memset( Byte* dst, Int32 filler, Int32 count );
          #endregion

          #endregion

          #region Image Related

          public static void SetGrayscalePalette( Bitmap image )
          {
               if ( image.PixelFormat != PixelFormat.Format8bppIndexed )
                    throw new ArgumentException( "Source image is not an 8 bpp image." );
               ColorPalette cp = image.Palette;
               for ( Int32 i = 0; i < 256; ++i )
               {
                    cp.Entries [ i ] = Color.FromArgb( i, i, i );
               }
               image.Palette = cp;
          }

          public static Bitmap CreateGrayscaleImage( int width, int height )
          {
               Bitmap img = new Bitmap( width, height, PixelFormat.Format8bppIndexed );
               SetGrayscalePalette( img );
               return img;
          }

          public static Bitmap Clone( Bitmap source, PixelFormat format )
          {
               // copy image if pixel format is the same
               if ( source.PixelFormat == format )
                    return Clone( source );

               Int32 width = source.Width;
               Int32 height = source.Height;

               // create new image with desired pixel format
               Bitmap bitmap = new Bitmap( width, height, format );

               // draw source image on the new one using Graphics
               Graphics g = Graphics.FromImage( bitmap );
               g.DrawImage( source, 0, 0, width, height );
               g.Dispose();

               return bitmap;
          }

          public static Bitmap Clone( Bitmap source )
          {
               // lock source bitmap data
               BitmapData sourceData = source.LockBits(
                   new Rectangle( 0, 0, source.Width, source.Height ),
                   ImageLockMode.ReadOnly, source.PixelFormat );

               // create new image
               Bitmap destination = Clone( sourceData );

               // unlock source image
               source.UnlockBits( sourceData );

               //
               if (
                   (source.PixelFormat == PixelFormat.Format1bppIndexed) ||
                   (source.PixelFormat == PixelFormat.Format4bppIndexed) ||
                   (source.PixelFormat == PixelFormat.Format8bppIndexed) ||
                   (source.PixelFormat == PixelFormat.Indexed) )
               {
                    ColorPalette srcPalette = source.Palette;
                    ColorPalette dstPalette = destination.Palette;

                    int n = srcPalette.Entries.Length;

                    // copy pallete
                    for ( int i = 0; i < n; i++ )
                    {
                         dstPalette.Entries [ i ] = srcPalette.Entries [ i ];
                    }

                    destination.Palette = dstPalette;
               }

               return destination;
          }

          public static Bitmap Clone( BitmapData sourceData )
          {
               // get source image size
               Int32 width = sourceData.Width;
               Int32 height = sourceData.Height;

               // create new image
               Bitmap destination = new Bitmap( width, height, sourceData.PixelFormat );

               // lock destination bitmap data
               BitmapData destinationData = destination.LockBits(
                   new Rectangle( 0, 0, width, height ),
                   ImageLockMode.ReadWrite, destination.PixelFormat );

               Native.CopyUnmanagedMemory( destinationData.Scan0, sourceData.Scan0, height * sourceData.Stride );

               // unlock destination image
               destination.UnlockBits( destinationData );

               return destination;
          }

          #endregion
     }
}
