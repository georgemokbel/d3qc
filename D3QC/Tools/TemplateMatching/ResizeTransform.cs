﻿// Slightly modified and stripped down version of AForge.net
// http://www.aforgenet.com/framework/

namespace TemplateMatching
{
     using System;
     using System.Collections.Generic;
     using System.Drawing;
     using System.Drawing.Imaging;
     public class ResizeTransform
     {
          #region Fields
          private Int32 _newWidth;
          private Int32 _newHeight;
          private Dictionary<PixelFormat, PixelFormat> _fmtTranslations = new Dictionary<PixelFormat, PixelFormat>();
          #endregion

          #region Properties

          public Dictionary<PixelFormat, PixelFormat> FormatTranslations
          {
               get { return _fmtTranslations; }
          }

          public Int32 NewWidth
          {
               get { return _newWidth; }
               set { _newWidth = Math.Max( 1, value ); }
          }

          public Int32 NewHeight
          {
               get { return _newHeight; }
               set { _newHeight = Math.Max( 1, value ); }
          }
          #endregion

          #region Constructor
          public ResizeTransform( Int32 width, Int32 height )
          {
               _newHeight = height;
               _newWidth = width;
               _fmtTranslations [ PixelFormat.Format8bppIndexed ] = PixelFormat.Format8bppIndexed;
               _fmtTranslations [ PixelFormat.Format24bppRgb ] = PixelFormat.Format24bppRgb;
          }
          #endregion

          #region Methods

          private Size CalculateNewImageSize( UImage src )
          {
               return new Size( _newWidth, _newHeight );
          }

          private unsafe void ProcessFilter( UImage sourceData, UImage destinationData )
          {
               // get source image size
               Int32 width = sourceData.Width;
               Int32 height = sourceData.Height;

               Int32 pixelSize = (sourceData.PixelFormat == PixelFormat.Format8bppIndexed) ? 1 : 3;
               Int32 srcStride = sourceData.Stride;
               Int32 dstOffset = destinationData.Stride - pixelSize * _newWidth;
               Double xFactor = ( Double )width / _newWidth;
               Double yFactor = ( Double )height / _newHeight;

               // do the job
               Byte* src = ( Byte* )sourceData.Data.ToPointer();
               Byte* dst = ( Byte* )destinationData.Data.ToPointer();

               // coordinates of source points and cooefficiens
               Double ox, oy, dx, dy, k1, k2;
               Int32 ox1, oy1, ox2, oy2;
               // destination pixel values
               Double r, g, b;
               // width and height decreased by 1
               Int32 ymax = height - 1;
               Int32 xmax = width - 1;
               // temporary pointer
               Byte* p;

               // check pixel format
               if ( destinationData.PixelFormat == PixelFormat.Format8bppIndexed )
               {
                    // grayscale
                    for ( Int32 y = 0; y < _newHeight; y++ )
                    {
                         // Y coordinates
                         oy = ( Double )y * yFactor - 0.5;
                         oy1 = ( Int32 )oy;
                         dy = oy - ( Double )oy1;

                         for ( Int32 x = 0; x < _newWidth; x++, dst++ )
                         {
                              // X coordinates
                              ox = ( Double )x * xFactor - 0.5f;
                              ox1 = ( Int32 )ox;
                              dx = ox - ( Double )ox1;

                              // initial pixel value
                              g = 0;

                              for ( Int32 n = -1; n < 3; n++ )
                              {
                                   // get Y cooefficient
                                   k1 = BiCubicKernel( dy - ( Double )n );

                                   oy2 = oy1 + n;
                                   if ( oy2 < 0 )
                                        oy2 = 0;
                                   if ( oy2 > ymax )
                                        oy2 = ymax;

                                   for ( Int32 m = -1; m < 3; m++ )
                                   {
                                        // get X cooefficient
                                        k2 = k1 * BiCubicKernel( ( Double )m - dx );

                                        ox2 = ox1 + m;
                                        if ( ox2 < 0 )
                                             ox2 = 0;
                                        if ( ox2 > xmax )
                                             ox2 = xmax;

                                        g += k2 * src [ oy2 * srcStride + ox2 ];
                                   }
                              }
                              *dst = ( Byte )Math.Max( 0, Math.Min( 255, g ) );
                         }
                         dst += dstOffset;
                    }
               }
               else
               {
                    // RGB
                    for ( Int32 y = 0; y < _newHeight; y++ )
                    {
                         // Y coordinates
                         oy = ( Double )y * yFactor - 0.5f;
                         oy1 = ( Int32 )oy;
                         dy = oy - ( Double )oy1;

                         for ( Int32 x = 0; x < _newWidth; x++, dst += 3 )
                         {
                              // X coordinates
                              ox = ( Double )x * xFactor - 0.5f;
                              ox1 = ( Int32 )ox;
                              dx = ox - ( Double )ox1;

                              // initial pixel value
                              r = g = b = 0;

                              for ( Int32 n = -1; n < 3; n++ )
                              {
                                   // get Y cooefficient
                                   k1 = BiCubicKernel( dy - ( Double )n );

                                   oy2 = oy1 + n;
                                   if ( oy2 < 0 )
                                        oy2 = 0;
                                   if ( oy2 > ymax )
                                        oy2 = ymax;

                                   for ( Int32 m = -1; m < 3; m++ )
                                   {
                                        // get X cooefficient
                                        k2 = k1 * BiCubicKernel( ( Double )m - dx );

                                        ox2 = ox1 + m;
                                        if ( ox2 < 0 )
                                             ox2 = 0;
                                        if ( ox2 > xmax )
                                             ox2 = xmax;

                                        // get pixel of original image
                                        p = src + oy2 * srcStride + ox2 * 3;

                                        r += k2 * p [ RGB.R ];
                                        g += k2 * p [ RGB.G ];
                                        b += k2 * p [ RGB.B ];
                                   }
                              }

                              dst [ RGB.R ] = ( Byte )Math.Max( 0, Math.Min( 255, r ) );
                              dst [ RGB.G ] = ( Byte )Math.Max( 0, Math.Min( 255, g ) );
                              dst [ RGB.B ] = ( Byte )Math.Max( 0, Math.Min( 255, b ) );
                         }
                         dst += dstOffset;
                    }
               }
          }

          private Double BiCubicKernel( Double x )
          {
               if ( x < 0 )
               {
                    x = -x;
               }

               Double biCoef = 0;

               if ( x <= 1 )
               {
                    biCoef = (1.5 * x - 2.5) * x * x + 1;
               }
               else if ( x < 2 )
               {
                    biCoef = ((-0.5 * x + 2.5) * x - 4) * x + 2;
               }

               return biCoef;
          }

          public Bitmap Apply( Bitmap image )
          {
               // lock source bitmap data
               BitmapData srcData = image.LockBits(
                   new Rectangle( 0, 0, image.Width, image.Height ),
                   ImageLockMode.ReadOnly, image.PixelFormat );

               Bitmap dstImage = null;

               try
               {
                    // apply the filter
                    dstImage = Apply( srcData );
                    if ( (image.HorizontalResolution > 0) && (image.VerticalResolution > 0) )
                    {
                         dstImage.SetResolution( image.HorizontalResolution, image.VerticalResolution );
                    }
               }
               finally
               {
                    // unlock source image
                    image.UnlockBits( srcData );
               }

               return dstImage;
          }

          public Bitmap Apply( BitmapData imageData )
          {
               // check pixel format of the source image
               CheckSourceFormat( imageData.PixelFormat );

               // destination image format
               PixelFormat dstPixelFormat = FormatTranslations [ imageData.PixelFormat ];

               // get new image size
               Size newSize = CalculateNewImageSize( new UImage( imageData ) );

               // create new image of required format
               Bitmap dstImage = (dstPixelFormat == PixelFormat.Format8bppIndexed) ?
                   Native.CreateGrayscaleImage( newSize.Width, newSize.Height ) :
                   new Bitmap( newSize.Width, newSize.Height, dstPixelFormat );

               // lock destination bitmap data
               BitmapData dstData = dstImage.LockBits(
                   new Rectangle( 0, 0, newSize.Width, newSize.Height ),
                   ImageLockMode.ReadWrite, dstPixelFormat );

               try
               {
                    // process the filter
                    ProcessFilter( new UImage( imageData ), new UImage( dstData ) );
               }
               finally
               {
                    // unlock destination images
                    dstImage.UnlockBits( dstData );
               }

               return dstImage;
          }

          public UImage Apply( UImage image )
          {
               // check pixel format of the source image
               CheckSourceFormat( image.PixelFormat );

               // get new image size
               Size newSize = CalculateNewImageSize( image );

               // create new destination image
               UImage dstImage = UImage.Create( newSize.Width, newSize.Height, FormatTranslations [ image.PixelFormat ] );

               // process the filter
               ProcessFilter( image, dstImage );

               return dstImage;
          }

          public void Apply( UImage sourceImage, UImage destinationImage )
          {
               // check pixel format of the source and destination images
               CheckSourceFormat( sourceImage.PixelFormat );

               // ensure destination image has correct format
               if ( destinationImage.PixelFormat != FormatTranslations [ sourceImage.PixelFormat ] )
               {
                    throw new ArgumentException( "Destination pixel format is specified incorrectly." );
               }

               // get new image size
               Size newSize = CalculateNewImageSize( sourceImage );

               // ensure destination image has correct size
               if ( (destinationImage.Width != newSize.Width) || (destinationImage.Height != newSize.Height) )
               {
                    throw new ArgumentException( "Destination image must have the size expected by the filter." );
               }

               // process the filter
               ProcessFilter( sourceImage, destinationImage );
          }

          private void CheckSourceFormat( PixelFormat pixelFormat )
          {
               if ( !FormatTranslations.ContainsKey( pixelFormat ) )
                    throw new ArgumentException( "Source pixel format is not supported by the filter." );
          }

          #endregion
     }
}
