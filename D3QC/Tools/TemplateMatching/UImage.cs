﻿// Slightly modified and stripped down version of AForge.net
// http://www.aforgenet.com/framework/

namespace TemplateMatching
{
     using System;
     using System.Drawing;
     using System.Drawing.Imaging;
     using System.Collections.Generic;

     /// <summary>
     /// An unmanaged image in memory.
     /// </summary>
     public class UImage : IDisposable
     {
          #region Fields
          private IntPtr _data;    // pointer to image data
          private Int32 _width, _height;     // width / heigt of image
          private Int32 _stride;             // image stride
          private PixelFormat _pixelFormat;  // image pixel format
          private Boolean _shouldDispose = false;
          #endregion

          #region Properties
          /// <summary>
          /// Pointer to image data.
          /// </summary>
          public IntPtr Data
          {
               get
               {
                    return _data;
               }
          }

          /// <summary>
          /// Image pixel width
          /// </summary>
          public Int32 Width
          {
               get
               {
                    return _width;
               }
          }

          /// <summary>
          /// Image pixel height
          /// </summary>
          public Int32 Height
          {
               get
               {
                    return _height;
               }
          }

          /// <summary>
          /// Image stride ( line size in bytes )
          /// </summary>
          public Int32 Stride
          {
               get
               {
                    return _stride;
               }
          }

          /// <summary>
          /// Images pixel format.
          /// </summary>
          public PixelFormat PixelFormat
          {
               get
               {
                    return _pixelFormat;
               }
          }

          #endregion

          #region Constructor / Destructor / Diposing
          /// <summary>
          /// Initialize a new instance of <see cref="UImage"/> class.
          /// </summary>
          /// <param name="data">Pointer to image data.</param>
          /// <param name="width">Pixel width of image.</param>
          /// <param name="height">Pixel height of image.</param>
          /// <param name="stride">Image stride ( size of line in bytes )</param>
          /// <param name="pixelFormat">Image pixel format.</param>
          public UImage( IntPtr data, Int32 width, Int32 height, Int32 stride, PixelFormat pixelFormat )
          {
               _data = data;
               _width = width;
               _height = height;
               _stride = stride;
               _pixelFormat = pixelFormat;
          }

          /// <summary>
          /// Initializes a new instance of <see cref="UImage"/> class.
          /// </summary>
          /// <param name="bitmapData">Locked bitmap data.</param>
          /// <remarks><note>This constructor does not make a copy of the managed image, therefore the bitmap must remain locked during operations.</note></remarks>
          public UImage( BitmapData bitmapData )
          {
               _data = bitmapData.Scan0;
               _width = bitmapData.Width;
               _height = bitmapData.Height;
               _stride = bitmapData.Stride;
               _pixelFormat = bitmapData.PixelFormat;
          }
          /// <summary>
          /// Destroys the instance of <see cref="UImage"/> class.
          /// </summary>
          ~UImage()
          {
               Dipose( false );
          }

          public void Dispose()
          {
               Dipose( true );
               GC.SuppressFinalize( this );
          }

          protected virtual void Dipose( Boolean disposing )
          {
               if ( disposing )
               {
                    // diposes
               }

               if ( (_shouldDispose) && (_data != IntPtr.Zero) )
               {
                    System.Runtime.InteropServices.Marshal.FreeHGlobal( _data );
                    System.GC.RemoveMemoryPressure( _stride * _height );
                    _data = IntPtr.Zero;
               }
          }
          #endregion

          #region Methods

          public UImage Clone()
          {
               IntPtr data = System.Runtime.InteropServices.Marshal.AllocHGlobal( _stride * _height );
               System.GC.AddMemoryPressure( _stride * _height );

               UImage uimg = new UImage( data, _width, _height, _stride, _pixelFormat );
               uimg._shouldDispose = true;

               Native.CopyUnmanagedMemory( data, _data, _stride * _height );

               return uimg;
          }

          public void Copy( UImage dst )
          {
               if ( (_width != dst._width) || (_height != dst._height) || (_pixelFormat != dst._pixelFormat) )
               {
                    throw new ArgumentException( "Destination image has different dimensions or pixel format." );
               }
               if ( _stride == dst._stride )
               {
                    Native.CopyUnmanagedMemory( dst._data, _data, _stride * _height );
               }
               else
               {
                    unsafe
                    {
                         Int32 dstStride = dst._stride;
                         Int32 len = (_stride < dstStride) ? _stride : dstStride;
                         Byte* src = ( Byte* )_data.ToPointer();
                         Byte* bdst = ( Byte* )dst._data.ToPointer();

                         for ( Int32 i = 0; i < _height; ++i )
                         {
                              Native.CopyUnmanagedMemory( bdst, src, len );
                              bdst += dstStride;
                              src += _stride;
                         }
                    }
               }
          }

          public static UImage Create( Int32 width, Int32 height, PixelFormat pixelFormat )
          {
               Int32 bpp = 0;

               // calculate bytes per pixel
               switch ( pixelFormat )
               {
                    case PixelFormat.Format8bppIndexed:
                         bpp = 1;
                         break;
                    case PixelFormat.Format16bppGrayScale:
                         bpp = 2;
                         break;
                    case PixelFormat.Format24bppRgb:
                         bpp = 3;
                         break;
                    case PixelFormat.Format32bppRgb:
                    case PixelFormat.Format32bppArgb:
                    case PixelFormat.Format32bppPArgb:
                         bpp = 4;
                         break;
                    case PixelFormat.Format48bppRgb:
                         bpp = 6;
                         break;
                    case PixelFormat.Format64bppArgb:
                    case PixelFormat.Format64bppPArgb:
                         bpp = 8;
                         break;
                    default:
                         throw new ArgumentException( "Can not create image with specified pixel format." );
               }
               // check size
               if ( (width <= 0) || (height <= 0) )
               {
                    throw new ArgumentOutOfRangeException( "Invalid image size." );
               }

               Int32 stride = width * bpp;
               if ( stride % 4 != 0 )
               {
                    stride += (4 - (stride % 4));
               }

               IntPtr imageData = System.Runtime.InteropServices.Marshal.AllocHGlobal( stride * height );
               Native.SetUnmanagedMemory( imageData, 0, stride * height );
               System.GC.AddMemoryPressure( stride * height );

               UImage img = new UImage( imageData, width, height, stride, pixelFormat );
               img._shouldDispose = true;

               return img;
          }

          public Bitmap ToManagedImage()
          {
               return ToManagedImage( true );
          }

          public Bitmap ToManagedImage( Boolean makeCopy )
          {
               Bitmap dstImage = null;
               try
               {
                    if ( !makeCopy )
                    {
                         dstImage = new Bitmap( _width, _height, _stride, _pixelFormat, _data );
                         if ( _pixelFormat == PixelFormat.Format8bppIndexed )
                         {
                              Native.SetGrayscalePalette( dstImage );
                         }

                    }
                    else
                    {
                         dstImage = (_pixelFormat == PixelFormat.Format8bppIndexed) ? Native.CreateGrayscaleImage( _width, _height ) : new Bitmap( _width, _height, _pixelFormat );
                         BitmapData dstData = dstImage.LockBits( new Rectangle( 0, 0, _width, _height ), ImageLockMode.ReadWrite, _pixelFormat );

                         int dstStride = dstData.Stride;
                         int lineSize = Math.Min( _stride, dstStride );

                         unsafe
                         {
                              Byte* dst = ( Byte* )dstData.Scan0.ToPointer();
                              Byte* src = ( Byte* )_data.ToPointer();

                              if ( _stride != dstStride )
                              {
                                   for ( Int32 y = 0; y < _height; ++y )
                                   {
                                        Native.CopyUnmanagedMemory( dst, src, lineSize );
                                        dst += dstStride;
                                        src += _stride;
                                   }
                              }
                              else
                              {
                                   Native.CopyUnmanagedMemory( dst, src, _stride * _height );
                              }
                         }
                         dstImage.UnlockBits( dstData );

                    }
                    return dstImage;
               }
               catch ( Exception )
               {
                    if ( dstImage != null )
                    {
                         dstImage.Dispose();
                    }
                    throw new InvalidOperationException( "Unmanaged image has invalid properties, failed to convert to managed image." );
               }
          }
          #endregion
     }
}

