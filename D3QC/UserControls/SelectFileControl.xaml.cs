﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

namespace D3QC
{
     /// <summary>
     /// Interaction logic for SelectFolderControl.xaml
     /// </summary>
     public partial class SelectFileControl : System.Windows.Controls.UserControl
     {
          public SelectFileControl()
          {
               InitializeComponent();
          }



          public static DependencyProperty TextProperty = DependencyProperty.Register( "Text", typeof( string ), typeof( SelectFileControl ), new FrameworkPropertyMetadata( null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault ) );
          public static DependencyProperty DescriptionProperty = DependencyProperty.Register( "Description", typeof( string ), typeof( SelectFileControl ), new PropertyMetadata( null ) );


          public string Text { get { return GetValue( TextProperty ) as string; } set { SetValue( TextProperty, value ); } }

          public string Description { get { return GetValue( DescriptionProperty ) as string; } set { SetValue( DescriptionProperty, value ); } }

     

          private void BrowseFile( object sender, RoutedEventArgs e )
          {
               using ( OpenFileDialog dlg = new OpenFileDialog() )
               {
                    dlg.FileName = Text;
                    
                    
                    DialogResult result = dlg.ShowDialog();
                    if ( result == System.Windows.Forms.DialogResult.OK )
                    {
                         Text = dlg.FileName;
                         BindingExpression be = GetBindingExpression( TextProperty );
                         if ( be != null )
                         {
                              be.UpdateSource();
                         }
                    }
               }
          }
     }
}
