﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace D3QC.View
{
     /// <summary>
     /// Interaction logic for MainView.xaml
     /// </summary>
     public partial class DefaultView : UserControl
     {
          public DefaultView()
          {
               InitializeComponent();
          }

          private void BackgroundWorker_DoWork( object sender, System.ComponentModel.DoWorkEventArgs e )
          {

          }

          private void BackgroundWorker_ProgressChanged( object sender, System.ComponentModel.ProgressChangedEventArgs e )
          {

          }

          private void BackgroundWorker_RunWorkerCompleted( object sender, System.ComponentModel.RunWorkerCompletedEventArgs e )
          {

          }
     }
}
