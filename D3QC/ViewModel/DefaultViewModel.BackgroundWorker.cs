﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.ObjectModel;
using D3QC;
using System.ComponentModel;

namespace D3QC.ViewModel
{
     public partial class DefaultViewModel
     {
          
          private void BackgroundWorker_DoWork( object sender, DoWorkEventArgs e )
          {
               RefreshButtonEnabled = false;
               CropButtonEnabled = false;
               CancelButtonEnabled = true;
               ApplicationStatus = "Status: Cropping";

               var ph = ( PhotoCropper )e.Argument;

               ph.ProcessImages( _bgWorker );

               if ( _bgWorker.CancellationPending )
               {
                    e.Cancel = true;
                    return;
               }

          }

          private void BackgroundWorker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
          {
               ProgressBarValue = 100;
               RefreshButtonEnabled = true;
               CropButtonEnabled = true;
               CancelButtonEnabled = false;
               ApplicationStatus = "Status: Idle";
               ProgressBarValue = 0;
          }

          // UserState structure : operation result @ log message 
          private void BackgroundWorker_ProgressChanged( object sender, ProgressChangedEventArgs e )
          {
               ProgressBarValue = (Int32)e.ProgressPercentage;
               String userState = (String)e.UserState;
               String [] results = userState.Split( '@' );
               MessengerInstance.Send( results[0] , D3QC.ViewModel.MessageType.ResultMessage );
               if( !(String.IsNullOrWhiteSpace( results[1] )) )
                    MessengerInstance.Send( results [ 1 ], D3QC.ViewModel.MessageType.LogMessage );
              
          }
     }
}
