﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.ObjectModel;
using D3QC;
using System.ComponentModel;
using System.Text;


namespace D3QC.ViewModel
{
     /// <remarks>
     /// This class contains a background worker implemented in DefaultViewModel.BackgroundWorker.cs
     /// </remarks>
     public partial class DefaultViewModel : ViewModelBase
     {
          #region Fields
          private Boolean _refreshButtonEnabled;
          private Boolean _cropButtonEnabled;
          private Boolean _cancelButtonEnabled;   
          private String _applicationStatus;
          private Int32 _progressBarValue;

          private D3QC.Model.PhotoModel _photoModel;

          private BackgroundWorker _bgWorker;
          #endregion

          #region Properties

          public D3QC.Model.PhotoModel PhotoModel
          { 
               get 
               { 
                    return _photoModel; 
               }
               set
               {
                    _photoModel = value;
               }
          }

          public ObservableCollection<String> FileList
          {
               get
               {
                    return _photoModel.FileList;
               }
               set
               {
                    _photoModel.FileList = value;
                    RaisePropertyChanged( "FileList" );
               }
          }

          public ObservableCollection<String> ProcessedFileList
          {
               get
               {
                    return _photoModel.ProcessedFileList;
               }
               set
               {
                    _photoModel.ProcessedFileList = value;
                    RaisePropertyChanged( "ProcessedFileList" );
               }
          }

          public String ApplicationStatus
          {
               get
               {
                    return _applicationStatus;
               }
               set
               {
                    _applicationStatus = value;
                    RaisePropertyChanged( "ApplicationStatus" );
               }
          }

          public Boolean RefreshButtonEnabled
          {
               get { return _refreshButtonEnabled; }
               set { _refreshButtonEnabled = value; RaisePropertyChanged( "RefreshButtonEnabled" ); }
          }

          public Boolean CancelButtonEnabled
          {
               get { return _cancelButtonEnabled; }
               set { _cancelButtonEnabled = value; RaisePropertyChanged( "CancelButtonEnabled" ); }
          }

          public Boolean CropButtonEnabled
          {
               get { return _cropButtonEnabled; }
               set { _cropButtonEnabled = value; RaisePropertyChanged( "CropButtonEnabled" ); }
          }

          public Int32 ProgressBarValue
          {
               get { return _progressBarValue; }
               set 
               {
                    if ( value > 100 )
                    {
                         _progressBarValue = 100;
                    }
                    else if ( value < 0 )
                    {
                         _progressBarValue = 0;
                    }
                    else
                         _progressBarValue = value;
                    RaisePropertyChanged( "ProgressBarValue" );
               }
          }
          #endregion

          #region Methods
          /// <summary>
          /// Sets crop/refresh enabled properties to respective values.
          /// </summary>
          /// <param name="val">The value of the IsEnabled property</param>
          private void SetButtonIsEnabled( Boolean val )
          {
               RefreshButtonEnabled = val;
               CropButtonEnabled = val;
          }

          /// <summary>
          /// Updates the list boxes.
          /// </summary>
          /// <param name="msg">The result of the operation on the most recently processed image.</param>
          private void AddToResultsListBox( String msg )
          {
               _photoModel.ProcessedFileList.Add( _photoModel.FileList[0] + " - " + msg );
               _photoModel.FileList.Remove( _photoModel.FileList [ 0 ] );
          }

          /// <summary>
          /// Checks to see if the D3Prefs contains a supported resolution.
          /// </summary>
          /// <returns>True if valid resolution.</returns>
          /// <remarks>Only 2560x1440, 1920x1200 and 1920x1080 are supported at the moment.</remarks>
          private Boolean IsUsingSupportedResolution()
          {
               Int32 width = D3QC.Model.D3PrefsModel.GetInstance.DisplayModeWidth;
               Int32 height = D3QC.Model.D3PrefsModel.GetInstance.DisplayModeHeight;
               if ( (width == 2560 && height == 1440) ||
                    (width == 1920 && ((height == 1200 || height == 1080))) )
                    return true;
               return false;
          }

          private void MoveFile( String fn )
          {
               // Move the original image.
               PhotoModel.FileList.Remove( fn );
               StringBuilder absoluteOriginalPath = new StringBuilder();
               StringBuilder moveLocation = new StringBuilder();
               absoluteOriginalPath.Append( ( String )Application.Current.Properties [ "ScreenshotPath" ] );
               absoluteOriginalPath.Append( fn );
               moveLocation.Append( ( String )Application.Current.Properties [ "OriginalsScreenshotPath" ] );
               moveLocation.Append( "\\" + DateTime.Now.ToString( "hhmm" ) + fn );
               // System.IO.File.Move( _absoluteCurrentFileName.ToString(), fn.ToString() );
               System.IO.File.Move( absoluteOriginalPath.ToString(), moveLocation.ToString() );
          }

          #endregion

          #region Constructor
          public DefaultViewModel()
          {
               _photoModel = new D3QC.Model.PhotoModel();

               _refreshButtonEnabled = true;
               _cropButtonEnabled = true;
               _cancelButtonEnabled = false;
               _applicationStatus = "Status: Idle";


               _bgWorker = new BackgroundWorker()
               {
                    WorkerReportsProgress = true,
                    WorkerSupportsCancellation = true
               };

               _bgWorker.DoWork += BackgroundWorker_DoWork;
               _bgWorker.ProgressChanged += BackgroundWorker_ProgressChanged;
               _bgWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;

               RefreshCommand = new RelayCommand( ExecuteRefreshCommand );
               CancelCommand = new RelayCommand(  ExecuteCancelCommand );
               CropCommand = new RelayCommand( ExecuteCropCommand );

               MessengerInstance.Register<Boolean>( this, MessageType.SettingsMessage, val => SetButtonIsEnabled( val ) );
               MessengerInstance.Register<String>( this, MessageType.ResultMessage, result => AddToResultsListBox( result ) );
               MessengerInstance.Register<String>( this, MessageType.MoveFile, fn => MoveFile( fn ) );

               ExecuteRefreshCommand();

          }
          #endregion

          #region Commands / Command Methods
          public RelayCommand RefreshCommand { get; private set; }
          public RelayCommand CancelCommand { get; private set; }
          public RelayCommand CropCommand { get; private set; }

          /// <summary>
          /// Refreshes the available screenshot list.
          /// </summary>
          private void ExecuteRefreshCommand()
          {
               ObservableCollection<String> fileList = new ObservableCollection<String>();
               DirectoryInfo dir = new DirectoryInfo( ( String )Application.Current.Properties [ "ScreenshotPath" ] );
               FileInfo [] files = dir.GetFiles( "*.jpg" );
               foreach ( FileInfo f in files )
               {
                    fileList.Add( f.Name );
                   
               }
               if ( fileList.Count > 0 )
               {
                    FileList = fileList;
                    
                    ProcessedFileList.Clear();
               }
               else
               {
                    MessengerInstance.Send( "Refreshed but found no files available for cropping.", MessageType.LogMessage );
               }
               
               
          }

          /// <summary>
          /// Cancels current cropping operation.
          /// </summary>
          private void ExecuteCancelCommand()
          {
               if( _bgWorker.IsBusy )
                    _bgWorker.CancelAsync();
          }

          /// <summary>
          /// Crops the current list of photos.
          /// </summary>
          private void ExecuteCropCommand()
          {
               if ( !IsUsingSupportedResolution() )
               {
                    MessageBox.Show( "Failed to execute cropping, see log tab for information.", "Error", MessageBoxButton.OK );
                    MessengerInstance.Send( "You are using an unsupported resolution.", MessageType.LogMessage );
               }
               else
               {
                    PhotoCropper ph = new PhotoCropper( _photoModel );
                    
                    _bgWorker.RunWorkerAsync( ph );
                    
               }        

          }

          
          
          #endregion
     }
}
