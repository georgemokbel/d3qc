﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using System;
using System.IO;

namespace D3QC.ViewModel
{
     public class LogViewModel : ViewModelBase
     {
          
          private ObservableCollection<string> _log = new ObservableCollection<string>();

          public RelayCommand SaveCommand { get; private set; }
          public RelayCommand ClearCommand { get; private set; }
          public RelayCommand BackCommand { get; private set; }

          public ObservableCollection<string> Log
          {
               get
               {
                    return _log;
               }
               set
               {
                    _log = value;
                    RaisePropertyChanged( "Log" );
               }

          }

          public LogViewModel()
          {
               SaveCommand = new RelayCommand( () => ExecuteSaveCommand() );
               ClearCommand = new RelayCommand( () => ExecuteClearCommand() );
               AddLogMessage( "Initialization" );

               MessengerInstance.Register<String>( this, MessageType.LogMessage, msg => AddLogMessage(msg) );
          }

          /// <summary>
          /// Saves the log file.
          /// </summary>
          private void ExecuteSaveCommand()
          {
               try
               {
                    String logFile = (DateTime.Now).ToString( ("M") ) + " - log.txt";
                    StreamWriter output = new StreamWriter( logFile, true, System.Text.UnicodeEncoding.Default );
                    foreach ( var s in Log )
                    {
                         output.WriteLine( s );
                    }

                    if ( output != null )
                    {
                         output.Close();
                         output = null;
                    }
               }
               catch ( Exception ex )
               {
                    AddLogMessage( "There was an error saving the log file: " + ex.ToString() );
               }
               
          }

          /// <summary>
          /// Clears the log file.
          /// </summary>
          private void ExecuteClearCommand()
          {
               // Erase the data in the LogViewModel
               Log.Clear();
          }

          /// <summary>
          /// Adds a message to the log.
          /// </summary>
          /// <param name="s"></param>
          public void AddLogMessage( String message, String format = "hh:mm" )
          {
               String formattedMessage = (DateTime.Now).ToString( format ) + " - " + message;
               Log.Add( formattedMessage );
          }

          
     }

}
