﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace D3QC.ViewModel
{
     public partial class MainViewModel : ViewModelBase
     {
         #region Properties

         public RelayCommand DefaultViewCommand { get; private set; }
         public RelayCommand SettingsViewCommand { get; private set; }
         public RelayCommand HelpViewCommand { get; private set; }
         public RelayCommand LogViewCommand { get; private set; }
         public RelayCommand AboutViewCommand { get; private set; }

         public RelayCommand BackCommand { get; private set; }
         public RelayCommand QuitCommand { get; private set; }
         public RelayCommand GetHelpCommand { get; private set; }

         #endregion

         #region Methods

         /// <summary>
         /// Set view to Default View.
         /// </summary>
         private void ExecuteDefaultViewCommand()
         {
              if ( CurrentViewModel != MainViewModel._defaultViewModel )
              {
                   CurrentWindowTitle = "D3QC";
                   CurrentViewModel = MainViewModel._defaultViewModel;
              }
         }

         /// <summary>
         /// Set view to Settings View.
         /// </summary>
         private void ExecuteSettingsViewCommand()
         {
              if ( CurrentViewModel != MainViewModel._settingsViewModel )
              {
                   CurrentWindowTitle = "D3QC - Settings";
                   CurrentViewModel = MainViewModel._settingsViewModel;
                   
              }
         }

         /// <summary>
         /// Set view to Help View.
         /// </summary>
         private void ExecuteHelpViewCommand()
         {
              CurrentWindowTitle = "D3QC - Help";
              CurrentViewModel = MainViewModel._helpViewModel;
         }

         /// <summary>
         /// Set view to Log View.
         /// </summary>
         private void ExecuteLogViewCommand()
         {
              if ( CurrentViewModel != MainViewModel._logViewModel )
              {
                   CurrentWindowTitle = "D3QC - Log";
                   CurrentViewModel = MainViewModel._logViewModel;
              }
         }

          /// <summary>
          /// Set view to AboutView
          /// </summary>
         private void ExecuteAboutViewCommand()
         {
              if ( CurrentViewModel != MainViewModel._aboutViewModel )
              {
                   CurrentWindowTitle = "D3QC - About";
                   CurrentViewModel = MainViewModel._aboutViewModel;
              }
         }

         /// <summary>
         /// Quit the application.
         /// </summary>
         private void ExecuteQuitCommand()
         {
              Application.Current.Shutdown();
         }

            /// <summary>
          /// Shows the help window.
          /// </summary>
         private void ExecuteGetHelpCommand()
         {

         }

         
         #endregion
     }
}
