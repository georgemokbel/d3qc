using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace D3QC.ViewModel
{
     // Message types used by the default messenger, inter-vm/tools communication.
     public enum MessageType { LogMessage, SettingsMessage, ResultMessage, MoveFile };
     
     

    /// <summary>
    /// This class contains properties that the Main View can data bind to. A bunch of child views happen here.
    /// </summary>
    /// <remarks>
    /// Commands and their related methods are located in MainViewModel.Commands.cs
    /// </remarks>
     public partial class MainViewModel : ViewModelBase
     {
          #region Fields

          private ViewModelBase _currentViewModel;
          private string _currentWindowTitle;

          readonly static DefaultViewModel _defaultViewModel = new DefaultViewModel();
          readonly static SettingsViewModel _settingsViewModel = new SettingsViewModel();
          readonly static LogViewModel _logViewModel = new LogViewModel();
          readonly static HelpViewModel _helpViewModel = new HelpViewModel();
          readonly static AboutViewModel _aboutViewModel = new AboutViewModel();

         
          #endregion

          #region Properties

          public ViewModelBase CurrentViewModel
          {
               get { return _currentViewModel; }
               set
               {
                    if ( _currentViewModel == value )
                         return;
                    _currentViewModel = value;
                    RaisePropertyChanged( "CurrentViewModel" );
               }
          }

          public string CurrentWindowTitle
          {
               get { return _currentWindowTitle; }
               set
               {
                    _currentWindowTitle = value;
                    RaisePropertyChanged( "CurrentWindowTitle" );
                    
               }
          }

          #endregion

          #region Constructor
          /// <summary>
          /// Initializes a new instance of the MainViewModel class.
          /// </summary>
          public MainViewModel()
          {
               CurrentViewModel = MainViewModel._defaultViewModel;
               CurrentWindowTitle = "D3QC";
               SetCommands();
               
          }

          #endregion


          #region Methods
          /// <summary>
          /// Sets all the commands.
          /// </summary>
          internal void SetCommands()
          {
               DefaultViewCommand = new RelayCommand( () => ExecuteDefaultViewCommand() );
               BackCommand = new RelayCommand( () => ExecuteDefaultViewCommand() );
               SettingsViewCommand = new RelayCommand( () => ExecuteSettingsViewCommand() );
               HelpViewCommand = new RelayCommand( () => ExecuteHelpViewCommand() );
               LogViewCommand = new RelayCommand( () => ExecuteLogViewCommand() );
               AboutViewCommand = new RelayCommand( () => ExecuteAboutViewCommand() );

               QuitCommand = new RelayCommand( () => ExecuteQuitCommand() );
               GetHelpCommand = new RelayCommand( () => ExecuteGetHelpCommand() );
          }
          #endregion
     }

    
}