﻿using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Windows.Data;
using D3QC;

namespace D3QC.ViewModel
{
     /// <summary>
     /// Class representing settings data in settings view.
     /// Currently data validation is implemented in each property. Change this.
     /// </summary>
     public class SettingsViewModel : ViewModelBase
     {
          #region Fields
          private D3QC.Model.SettingsModel _settings;
          
          #endregion

          
          #region Properties
          public D3QC.Model.SettingsModel Settings
          {
               get { return _settings; } 
          }

      
          public string D3PrefsFile
          {
               get { return Settings.D3PrefsFile; }
               set
               {
                    if ( IsValidD3PrefsFile( value ) )
                    {
                         Settings.D3PrefsFile = value;
                         ValidD3PrefsFile = true;
                         RaisePropertyChanged( "D3PrefsFile" );
                    }
                    else
                    {
                         ValidD3PrefsFile = false;
                         MessengerInstance.Send( false, MessageType.SettingsMessage );
                    }
                    
               }
          }
 
          public string ScreenshotPath
          {
               get { return Settings.ScreenshotPath; }
               set
               {
                    if ( IsValidPath( ref value ) )
                    {
                         if ( EndsWithBackslash( value ) )
                         {
                              Settings.ScreenshotPath = value;
                         }
                         else
                         {
                              Settings.ScreenshotPath = value + "\\";
                         }
                         ValidScreenshotPath = true;
                         RaisePropertyChanged( "ScreenshotPath" );
                    }
                    else
                    {
                         ValidScreenshotPath = false;
                         MessengerInstance.Send( false, MessageType.SettingsMessage );
                    }
                    
               }
          }

          public string CroppedScreenshotPath
          {
               get { return Settings.CroppedScreenshotPath; }
               set
               {
                    if ( IsValidPath( ref value ) )
                    {
                         if ( EndsWithBackslash( value ) )
                         {
                              Settings.CroppedScreenshotPath = value;
                         }
                         else
                         {
                              Settings.CroppedScreenshotPath = value + "\\";
                         }
                         ValidCroppedScreenshotPath = true;
                         RaisePropertyChanged( "CroppedScreenshotPath" );
                    }
                    else
                    {
                         ValidCroppedScreenshotPath = false;
                         MessengerInstance.Send( false, MessageType.SettingsMessage );
                    }
               }
          }

          public string OriginalsScreenshotPath
          {
               get { return Settings.OriginalsScreenshotPath; }
               set
               {
                    if ( IsValidPath( ref value ) )
                    {
                         if ( EndsWithBackslash( value ) )
                         {
                              Settings.OriginalsScreenshotPath = value;
                         }
                         else
                         {
                              Settings.OriginalsScreenshotPath = value + "\\";
                         }
                         ValidOriginalsScreenshotPath = true;
                         RaisePropertyChanged( "OriginalsScreenshotPath" );
                    }
                    else
                    {
                         ValidOriginalsScreenshotPath = false;
                         MessengerInstance.Send( false, MessageType.SettingsMessage );
                    }
               }
          }

          public Boolean ValidScreenshotPath
          {
               get
               {
                    return Settings.ValidScreenshotPath;
               }
               set 
               { 
                    Settings.ValidScreenshotPath = value; 
                    RaisePropertyChanged( "ValidScreenshotPath" );
               }
          }
          public Boolean ValidCroppedScreenshotPath
          {
               get
               { 
                    return Settings.ValidCroppedScreenshotPath;
               }

               set 
               { 
                    Settings.ValidCroppedScreenshotPath = value; 
                    RaisePropertyChanged( "ValidCroppedScreenshotPath" ); 
               }
          }
          public Boolean ValidOriginalsScreenshotPath
          {
               get
               {
                    return Settings.ValidOriginalsScreenshotPath; 
               }
               set 
               { 
                    Settings.ValidOriginalsScreenshotPath = value;
                    RaisePropertyChanged( "ValidOriginalsScreenshotPath" ); 
               }
          }
          public Boolean ValidD3PrefsFile
          {
               get
               {
                    return Settings.ValidD3PrefsFile;
               }
               set 
               { 
                    Settings.ValidD3PrefsFile = value; 
                    RaisePropertyChanged( "ValidD3PrefsFile" ); 
               }
          }
          #endregion

          #region Constructor
          public SettingsViewModel()
          {
               _settings = new Model.SettingsModel();
               PopulateView();
               SaveSettingsCommand = new RelayCommand( ExecuteSaveSettingsCommand );           
          }
          #endregion

          #region Methods

          /// <summary>
          /// Checks to make sure settings are set and valid.
          /// </summary>
          private void PopulateView()
          {
               LoadStoredProperties();
          }

          /// <summary>
          /// Loads settings with previously stored application properties.
          /// </summary>
          private void LoadStoredProperties()
          {
               D3PrefsFile = ( String )Application.Current.Properties [ "D3PrefsFile" ];
               ValidD3PrefsFile = true;
               ScreenshotPath = (String) Application.Current.Properties [ "ScreenshotPath" ];
               ValidScreenshotPath = true;
               CroppedScreenshotPath = (String) Application.Current.Properties [ "CroppedScreenshotPath" ];
               ValidCroppedScreenshotPath = true;
               OriginalsScreenshotPath = (String) Application.Current.Properties [ "OriginalsScreenshotPath" ];
               ValidOriginalsScreenshotPath = true;

          }

          /// <summary>
          /// Checks to see if the path is valid.
          /// </summary>
          /// <param name="path">The path being checked.</param>
          /// <returns>True if valid path.</returns>
          private Boolean IsValidPath( ref String path )
          {
               if ( String.IsNullOrWhiteSpace( path ) || (!System.IO.Directory.Exists( path )) )
               {
                    return false;
               }
               else
               {
                    return true;
               }

          }

          /// <summary>
          /// Checks to see if the file is valid.
          /// </summary>
          /// <param name="file">The absolute file name.</param>
          /// <returns>True if the file is valid.</returns>
          private Boolean IsValidD3PrefsFile( String file )
          {
               if ( String.IsNullOrWhiteSpace( file ) || (!System.IO.File.Exists( file )) )
               {
                    return false;
               }
               else
               {

                    String [] results = file.Split( '\\' );

                    if ( results [ results.Length - 1 ] == "D3Prefs.txt" )
                    {
                         return true;
                    }
                    else
                    {
                         return false;
                    }
               }
          }

          /// <summary>
          /// Checks to see if the string ends with a backslash.
          /// </summary>
          /// <param name="path">The path to check.</param>
          /// <returns>True if string ends with backslash.</returns>
          private Boolean EndsWithBackslash( String path )
          {
               return (path.EndsWith( "\\" ));
          }

          #endregion

          #region Commands / Command Methods
          public RelayCommand SaveSettingsCommand { get; private set; }

          /// <summary>
          /// Saves the settings into application.current.properties, writes to configuration file.
          /// </summary>
          private void ExecuteSaveSettingsCommand()
          {          
               Application.Current.Properties [ "D3PrefsFile" ] = D3PrefsFile;
               Application.Current.Properties [ "ScreenshotPath" ] = ScreenshotPath;
               Application.Current.Properties [ "CroppedScreenshotPath" ] = CroppedScreenshotPath;
               Application.Current.Properties [ "OriginalsScreenshotPath" ] = OriginalsScreenshotPath;

               App.Utility.WriteSettings();
               MessengerInstance.Send( "Saved settings.", MessageType.LogMessage );
               MessengerInstance.Send( true, MessageType.SettingsMessage );
               App.Utility.LoadSettings();

          }
          #endregion
     }

     
}
