d3qc
====
Thanks for downloading D3QC.

Created by George Mokbel, this program uses AForge and GalaSoft libraries. 

Please keep the file/folder structure the same as when it was downloaded.

All dll files should reside in the same folder level as D3QC.exe

If you cannot run the program you may need to download the Microsoft .NET framework version 4.5.1

Since AForge's license is GPL-3, I've made D3QC GPL-3.

The program is free to modify and redistribute. Source code is entirely viewable on my github @ https://github.com/gmokbel/d3qc